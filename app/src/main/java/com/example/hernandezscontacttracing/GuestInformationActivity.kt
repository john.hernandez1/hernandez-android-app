package com.example.hernandezscontacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hernandezscontacttracing.databinding.ActivityGuestInformationBinding

class GuestInformationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGuestInformationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGuestInformationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init(){
        binding.nextBtn.setOnClickListener{
            val intent = Intent(this, HealthInformationActivity::class.java)
            intent.putExtra("fullname",binding.fullNameText.text.toString())
            intent.putExtra("mobile",binding.mobileNumberText.text.toString())
            intent.putExtra("city",binding.cityText.text.toString())
            startActivity(intent)
        }
    }

}



