package com.example.hernandezscontacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hernandezscontacttracing.databinding.ActivityGuestInformationBinding
import com.example.hernandezscontacttracing.databinding.ActivityHealthInformationBinding

class HealthInformationActivity : AppCompatActivity(){

    private lateinit var binding: ActivityHealthInformationBinding

    private var listTasks: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHealthInformationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init(){
        var fullname = intent.extras?.getString("fullname")
        val mobile = intent.extras?.getString("mobile")
        val city = intent.extras?.getString("city")
        binding.nextBtn2.setOnClickListener{
            val intent = Intent(this, ConfirmationActivity::class.java)
            intent.putExtra("fullname",fullname.toString())
            intent.putExtra("mobile",mobile.toString())
            intent.putExtra("city",city.toString())
            intent.putExtra("symptoms",binding.symptomsText.text.toString())
            intent.putExtra("contact",binding.contactText.text.toString())
            startActivity(intent)
        }
    }
}