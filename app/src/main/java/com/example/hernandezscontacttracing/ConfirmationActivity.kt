package com.example.hernandezscontacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hernandezscontacttracing.databinding.ActivityConfirmationBinding


class ConfirmationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityConfirmationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConfirmationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }


    private fun init() {
        setUpRecyclerView()
        binding.confirmBtn.setOnClickListener {
            val intent = Intent(this,SucessfulActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setUpRecyclerView()
    {
        var fullname = intent.extras?.getString("fullname")
        val mobile = intent.extras?.getString("mobile")
        val city = intent.extras?.getString("city")
        val symptom = intent.extras?.getString("symptoms")
        val contact = intent.extras?.getString("contact")
        binding.MyRecycleView.apply {
            adapter = MyRecycleViewAdaptor(
                fullname = fullname.toString(),
                mobile = mobile.toString(),
                city = city.toString(),
                symptom = symptom.toString(),
                contact = contact.toString()
            )
            layoutManager = LinearLayoutManager(context)
        }
    }

}