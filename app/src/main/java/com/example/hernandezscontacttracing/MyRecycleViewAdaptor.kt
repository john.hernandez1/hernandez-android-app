package com.example.hernandezscontacttracing

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.hernandezscontacttracing.databinding.ItemListBinding

class MyRecycleViewAdaptor(
    private val fullname : String,
    private val mobile: String,
    private val city: String,
    private val symptom: String,
    private val contact: String
    ) : RecyclerView.Adapter<MyRecycleViewAdaptor.ItemViewHolder>() {

    private var list : MutableList<String> = mutableListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = ItemListBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        list.add(fullname)
        list.add(mobile)
        list.add(city)
        list.add(symptom)
        list.add(contact)
        return ItemViewHolder(binding)
    }
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.binding.textViewItemTask.text = list[position]
    }
    override fun getItemCount(): Int {
        return 5;
    }

    class ItemViewHolder(val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root)

}